public class GameCharacter{
	//instance variables
	protected String name;
	protected int health;
	protected int attackPower;

	//Constructor
	public GameCharacter(String name, int health, int attackPower){
		this.name = name;
		this.health = health;
		this.attackPower = attackPower;
	}//end constructor 

	public void takeDamage(int attackPower){
		this.health = health - attackPower;
	}//end takedamage

	public String getName(){
		return name;
	}// end get name

	public int getAttackPower(){
		return attackPower;
	}//end get attack power

	public int getHealth(){
		return health;
	}//end get health

	@Override
    public String toString() {

        String val = "";

        val += "Name: " + name;
        val += " Health: " + health;
        val += " AttackPower: " + attackPower;
        return val;
    }


}//end player class