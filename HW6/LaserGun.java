public class LaserGun extends Moveable{
	protected int attackPower;

	public LaserGun(String name, int attackPower){
		super(name);
		this.attackPower = attackPower;
	}
@Override
    public String toString() {

        String val = super.toString();
     	val += "attackPower: " + attackPower;
        return val;
    }
}