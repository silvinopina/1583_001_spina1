public class Cannon extends Moveable{
	protected int attackPower;
	
	public Cannon(String name, int attackPower){
		super(name);
		this.attackPower = attackPower;
	}
@Override
    public String toString() {

        String val = super.toString();
     	val += "attackPower: " + attackPower;
        return val;
    }
}