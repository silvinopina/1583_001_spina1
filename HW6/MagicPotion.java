public class MagicPotion extends Moveable{
	protected int mana;

	public MagicPotion(String name, int mana){
		super(name);
		this.mana = mana;
	}
@Override
    public String toString() {

        String val = super.toString();
     	val += "Mana: " + mana;
        return val;
    }
}