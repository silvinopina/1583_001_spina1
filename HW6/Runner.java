import java.util.Scanner;
import java.security.SecureRandom;
/**
*
* author: Silvino Pina
*
* version: 10/31/17
*
*/

public class Runner{
	static SecureRandom randomNumbers = new SecureRandom();
    //main method 
	 public static void main(String[] args){
       Player userPlayer = new Player("Charlie", 100, 5, 3, 0);
       Monster scareCrow = new Monster("ScareCrow", 100, 2, 5);
       Monster pengiun = new Monster("Pengiun", 100, 5, 4);
     	
        //create dungeon for game 
	 	Dungeon map = new Dungeon();
		//setting current room
		Rooms currentroom = map.getRoom0();
  		//string for error
        String error = ("No Doorway there.");
        //Setting up user input for movement
        Scanner input = new Scanner(System.in);

        int luck = randomNumbers.nextInt(100);
        int health = userPlayer.getHealth();
       
        boolean Game = true;
        while (Game){

            //Print out current info and ask for input
            System.out.println(currentroom);
            System.out.println("Be CareFul. \nThere are monsters along the ways. \nTo win must gain 15 XP \nWhich direction? ");
            char response = input.nextLine().charAt(0);
        
        if(response == 'n'|| response == 'N'){
            
            if(currentroom.getNorth() != null){
                if(luck%2  == 0){
                    userPlayer.takeTurn(scareCrow);
                }
                else if (luck%2 != 0){
                    userPlayer.takeTurn(pengiun);
                }
                currentroom = currentroom.getNorth();
            }
            else{
                System.out.println(error);
            }
            
        }//end loop
        
         if(response == 'e'|| response == 'E'){
             
            if(currentroom.getEast() != null){
                if(luck%2 == 0){
                    userPlayer.takeTurn(scareCrow);
                 }   
                else if (luck%2 != 0){
                    userPlayer.takeTurn(pengiun);
                }
                currentroom = currentroom.getEast();
            }
            else{
                System.out.println(error);
            }
           

        }//end loop

        if(response == 's'|| response == 'S'){
            
            if(currentroom.getSouth() != null){
                if(luck%2 == 0){
                    userPlayer.takeTurn(scareCrow);
                }
                else if (luck%2 != 0){
                    userPlayer.takeTurn(pengiun);
                } 
                currentroom = currentroom.getSouth();
            }
            else{
                System.out.println(error);
            }

        }//end loop

        if(response == 'w'|| response == 'W'){
            
            if(currentroom.getWest() != null){
               if(luck%2 == 0){
                userPlayer.takeTurn(scareCrow);
                
                }
                else if (luck%2 != 0){
                    userPlayer.takeTurn(pengiun);
                } 
                currentroom = currentroom.getWest();
            }
            else{
                System.out.println(error);
            }


        }//end loop


        if(response == 'q' || response =='Q'){
            Game = false;
            System.out.println("You have decided to leave.");
        }
        
        else if(userPlayer.xP >= 15){
            Game = false;
            System.out.println("You win!!");
        }

        else if(userPlayer.getHealth()<= 0){
            Game = false;
            System.out.println("You lose!!");
        }
        
        }// End game loop

 	}//end main method
}//end class