import java.util.Scanner;
public class Player extends GameCharacter{
	protected int mana;
	protected int xP;

	//constructor
	public Player(String name, int health, int attackPower, int mana, int xP){
		super(name,health,attackPower);
		this.mana = mana;
		this.xP = xP;
	}//end constructor 

	//get health method
	public int getHealth(){
		return this.health;
	}
	
	//Attack method
	public void attack(Monster theMonster){
		theMonster.health = theMonster.health - this.attackPower;
	}

	//castSpell method
	public void castSpell(Monster theMonster){
		theMonster.health = theMonster.health - this.mana;
	}

	//charge mana method
	public void chargeMana(){
		this.mana = this.mana + 2;
	}

	//Take turn/ combat method
	public void takeTurn(Monster theMonster){
		Scanner input = new Scanner(System.in);
		
		//Set boolean for loop true
		boolean gameStatus = true;
			
			//Start Combat loop
			while (gameStatus){		
				/*Report Combat Stats*/
				System.out.println("Hero: HP: " + this.health + " MP: " + this.mana + " XP; " + this.xP);
				System.out.println("Monster: "+ theMonster.name + "HP: " + theMonster.health + " XP: " + theMonster.xP );
				/*Combat Menu*/
				System.out.println("What action do you want to perform?");
				//Option 1
				System.out.println("	1.) Attack");
				//Option 2
				System.out.println("	2.) Cast Spell");
				//Option 3
				System.out.println("	3.) Charge Mana");
				//Option 4
				System.out.println("	4.) Run Away");
		
			
				int action = input.nextInt();

				//Combat Calculation Loop Begins 
				if(action == 1){
					//Invoke Melee Method
					attack(theMonster);
					theMonster.attack(this);
				}

				else if (action == 2){
					//Invoke Magic Method
					castSpell(theMonster);
					theMonster.attack(this);
				}

				else if (action == 3){
					//Invoke Charge Method
					chargeMana();
					theMonster.attack(this);
				}

				else if (action == 4){
					//Flee Option
					gameStatus = false;
					System.out.println("You run away!");
				}

				else if (action < 1 || action > 4){
					//Invalid options 
					System.out.println("This is not a valid option");
				}

				if (this.health <= 0){
					gameStatus = false;
					System.out.println("You have been defeated by " + theMonster.name + "!");
				}

				if (theMonster.health <= 0){
					gameStatus = false;
					System.out.println("You have defeated the " + theMonster.name);
					this.xP = this.xP + theMonster.xP;
					theMonster.health = 100;

				}
		}// Combat Loop ends 

	}// end combat manager

	//Return mana
	public int getMana(){
		return mana;
	}

@Override
    public String toString() {

        String val = super.toString();
        val += "Mana: " + mana;
        return val;
    }//end to string

}//end class