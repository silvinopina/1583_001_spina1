public class Monster extends GameCharacter{
	//instance variable
	protected int xP;

	//constructor for monster
	public Monster(String name, int health, int attackPower, int xP){
		super(name,health,attackPower);
		this.xP = xP;
	}

	//Attack the player method
	public void attack(Player thePlayer){
		thePlayer.health = thePlayer.health - this.attackPower;
	}

	//return XP
	public int getXP(){
		return xP;
	}

	@Override
    public String toString() {

        String val = super.toString();
        val += "xP: " + xP;
        return val;
    }//end toString

}//end class