/**
*
* author: Silvino Pina
*
* version: 10/31/17
*
*/

public class Rooms{
	//Setting up instance variables
	private String description;
	private Rooms North;
	private Rooms East;
	private Rooms South;
	private Rooms West;

	//Rooms constructor
	public Rooms(String description){
		this.description = description;
		this.North = null;
		this.East = null;
		this.South = null;
		this.West = null;
	}

	//Setting North
	public void setNorth(Rooms room){
		this.North = room;
	}

	//Setting South
	public void setEast(Rooms room){
		this.East = room;
	}

	//Setting South
	public void setSouth(Rooms room){
		this.South = room;
	}
	
	// Setting West
	public void setWest(Rooms room){
		this.West = room;
	}
	
	//returning Description
	public String getDescription(){
		return description;
	}

	//Return north
	public Rooms getNorth(){
		return this.North;
	}

	//Return East 
	public Rooms getEast(){
		return this.East;
	}

	//Return South
	public Rooms getSouth(){
		return this.South;
	}

	//Return West
	public Rooms getWest(){
		return this.West;
	}

	 @Override
    public String toString() {

        String val = "You are currently in " + getDescription();
        val += "\nExits are: ";
        if (getNorth() != null)
            val += " N ";
        if (getSouth() != null)
            val += " S ";
        if (getEast() != null)
            val += " E ";
        if (getWest() != null)
            val += " W";
        return val;
    }
}