import java.util.Scanner;
/**
*
* author: Silvino Pina
*
* version: 10/31/17
*
*/

public class Dungeon{
    //Setting intialize variables 
        private Rooms GuestBedroom;
        private Rooms SouthHall;
        private Rooms DiningRoom; 
        private Rooms MasterBedRoom;
        private Rooms NorthHall;
        private Rooms Kitchen;
        private Rooms Balcony;
	 
     public Dungeon(){
	 	//giving rooms the values 
        GuestBedroom = new Rooms("You are in the guest bedroom.");
		SouthHall = new Rooms("You are in the South Hall.");
        DiningRoom = new Rooms("You are in the Dining Room.");
		MasterBedRoom = new Rooms("You are in the Master Bedroom.");
		NorthHall = new Rooms("You are in the North Hall.");
		Kitchen = new Rooms("You are in the Kitchen.");
		Balcony = new Rooms("You are in the Balcony.");
    
        //setting rooms 
      	GuestBedroom.setNorth(MasterBedRoom);
        GuestBedroom.setEast(SouthHall);
        SouthHall.setNorth(NorthHall);
        SouthHall.setWest(GuestBedroom);
        SouthHall.setEast(DiningRoom);
        DiningRoom.setNorth(Kitchen);
        DiningRoom.setWest(SouthHall);
        MasterBedRoom.setSouth(GuestBedroom);
        MasterBedRoom.setEast(NorthHall);
        NorthHall.setWest(MasterBedRoom);
        NorthHall.setSouth(SouthHall);
        NorthHall.setEast(Kitchen);
        NorthHall.setNorth(Balcony);
        Kitchen.setWest(NorthHall);
        Kitchen.setSouth(DiningRoom);

    }
    //returning start room
    public Rooms getRoom0(){
        return GuestBedroom;

    }//end method
 
       
}//end class