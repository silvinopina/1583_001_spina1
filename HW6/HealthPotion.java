public class HealthPotion extends Moveable{
	protected int health;

	public HealthPotion(String name, int health){
		super(name);
		this.health = health;
	}
@Override
    public String toString() {

        String val = super.toString();
     	val += "Health: " + health;
        return val;
    }
}