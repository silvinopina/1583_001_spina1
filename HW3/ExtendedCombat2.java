import java.security.SecureRandom;
import java.util.Scanner;
/**
*A Program in which a hero is created to fight of random monsters
*
* Author: Silvino Pina
* Date: 9/22/17
*/

public class ExtendedCombat2 {
	private static int health;
	private static int attackPower;
	private static int magicPower;
	private static int statPoints = 20;
	private static String monsterName;
	private static int monsterHealth;
	private static int monsterAttack;
	private static int monsterXP;
	private static SecureRandom randomNumbers = new SecureRandom();
		
	public static void main(String[] args){
		heroCreation();

		monsterCreation();

	} //end main method

	//Hero Creation method
	public static void heroCreation(){
		
		//Loop while the player still has stat points
		while (statPoints > 0){

		//Print Hero Stats
		System.out.println("Health:" + health + ", Attack:" + attackPower + ", Magic:" + magicPower);

		//Print Menu
		System.out.println("1.) 	+10 Helath");
		System.out.println("2.) 	+1 Attack Power");
		System.out.println("3.) 	+3 Magic Power");
		
		Scanner input = new Scanner(System.in);
		System.out.println("You have " + statPoints + " points to spend");
		int Points = input.nextInt();

		if (Points == 1){
			health = health + 10;
			statPoints = statPoints - 1;
		}

		else if (Points == 2){
			attackPower = attackPower +1;
			statPoints = statPoints - 1;
		}

		else if (Points == 3){
			magicPower = magicPower + 3;
			statPoints = statPoints - 1;

		}

		else {
			System.out.println("Invalid Choice try again.");
		}

		}
	} //end Hero creation method
	public static void monsterCreation(){
		
		int Monster = randomNumbers.nextInt(3);
		


		if (Monster == 0){
			System.out.println("You have encountered a Goblin!");
			monsterHealth = 75 + randomNumbers.nextInt(25);
			monsterAttack = 8 + randomNumbers.nextInt(5);
			monsterXP = 1;
		}

		else if (Monster == 1){
			System.out.println("You have encountered an Ork!");
			monsterHealth = 100 + randomNumbers.nextInt(25);
			monsterAttack = 12+ randomNumbers.nextInt(5);
			monsterXP = 3;
		}

		else if (Monster == 2){
			System.out.println("You have encountered a Troll!");
			monsterHealth = 150 + randomNumbers.nextInt(50);
			monsterAttack = 15 + randomNumbers.nextInt(5);
			monsterXP = 5;
		}



	} //end monster creation
} //end class

