import java.util.Scanner;
/**
*A Program in which a hero is created to fight of random monsters
*
* Author: Silvino Pina
* Date: 9/22/17
*/

public class ExtendedCombat1 {
		
	public static void main(String[] args){
		monsterCreation();

	} //end main method

	//Monster Creation method
	private static void monsterCreation(){
		//Hero Stats
		int Health = 0; 
		int AttackPower = 0;
		int MagicPower = 0;
		int StatPoints = 20;
		
		//Loop while the player still has stat points
		while (StatPoints > 0){

		//Print Hero Stats
		System.out.println("Health:" + Health + ", Attack:" + AttackPower + ", Magic:" + MagicPower);

		//Print Menu
		System.out.println("1.) 	+10 Helath");
		System.out.println("2.) 	+1 Attack Power");
		System.out.println("3.) 	+3 Magic Power");
		
		Scanner input = new Scanner(System.in);
		System.out.println("You have " + StatPoints + " points to spend");
		int Points = input.nextInt();

		if (Points == 1){
			Health = Health + 10;
			StatPoints = StatPoints - 1;
		}

		else if (Points == 2){
			AttackPower = AttackPower +1;
			StatPoints = StatPoints - 1;
		}

		else if (Points == 3){
			MagicPower = MagicPower + 3;
			StatPoints = StatPoints - 1;

		}

		else {
			System.out.println("Invalid Choice try again.");
		}

		}
	} //end monster creation method
} //end class

