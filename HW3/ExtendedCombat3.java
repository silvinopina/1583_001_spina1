import java.security.SecureRandom;
import java.util.Scanner;
/**
*A Program in which a hero is created to fight of random monsters
*
* Author: Silvino Pina
* Date: 9/22/17
*/

public class ExtendedCombat3 {
	
	//Declare Variables
	private static int health;
	private static int attackPower = 0;
	private static int magicPower;
	private static int statPoints = 20;
	private static String monsterName;
	private static int monsterHealth;
	private static int monsterAttack = 0;
	private static int monsterXP;
	
	//Creating random 
	private static SecureRandom randomNumbers = new SecureRandom();
		
	//Start Main Method 
	public static void main(String[] args){
		
		//Invoke Methods
		heroCreation();
		monsterCreation();
		combatManage();

	} //end main method

	//Hero Creation method
	public static void heroCreation(){
		
		//Loop while the player still has stat points
		while (statPoints > 0){

			//Print Hero Stats
			System.out.println("Health:" + health + ", Attack:" + attackPower + ", Magic:" + magicPower);

			//Print Menu
			System.out.println("1.) 	+10 Health");
			System.out.println("2.) 	+1 Attack Power");
			System.out.println("3.) 	+3 Magic Power");
		
			//Create Scanner for Player Choice
			Scanner input = new Scanner(System.in);
			
			//Print out Stats
			System.out.println("You have " + statPoints + " points to spend");
			int Points = input.nextInt();

		//Loop Creation for Creating Hero 
		if (Points == 1){
			//Player Health Calculation
			health = health + 10;
			statPoints = statPoints - 1;
		}

		else if (Points == 2){
			//Player Attack Calculation
			attackPower = attackPower +1;
			statPoints = statPoints - 1;
		}

		else if (Points == 3){
			//Player Magic Calculation
			magicPower = magicPower + 3;
			statPoints = statPoints - 1;

		}

		else {
			//Player Enters invalid Choice
			System.out.println("Invalid Choice try again.");
		}

		}//end loop 

	} //end Hero creation method
	
	//Start Monster Creation Method
	public static void monsterCreation(){
		
		int Monster = randomNumbers.nextInt(3);
		

		//Start Monster Creation Loop
		if (Monster == 0){
			//Goblin Creation
			System.out.println("You have encountered a Goblin!");
			monsterName = ("Goblin");
			monsterHealth = 75 + randomNumbers.nextInt(25);
			monsterAttack = 8 + randomNumbers.nextInt(5);
			monsterXP = 1;
		}

		else if (Monster == 1){
			//Ork Creation 
			monsterName = ("Ork");
			System.out.println("You have encountered an Ork!");
			monsterHealth = 100 + randomNumbers.nextInt(25);
			monsterAttack = 12+ randomNumbers.nextInt(5);
			monsterXP = 3;
		}

		else if (Monster == 2){
			//Troll Creation
			monsterName = ("Troll");
			System.out.println("You have encountered a Troll!");
			monsterHealth = 150 + randomNumbers.nextInt(50);
			monsterAttack = 15 + randomNumbers.nextInt(5);
			monsterXP = 5;
		}//End loop

	} //end monster creation
	
	//Start Combat Managing Methof
	public static void combatManage(){
		//Scanner For user input 
		Scanner input = new Scanner(System.in);
		
		//Set boolean for loop true
		boolean gameStatus = true;
			
			//Start Combat loop
			while (gameStatus){		
				/*Report Combat Stats*/
		
				System.out.println( monsterName + ": HP: " + monsterHealth);
				System.out.println("Hero: HP: " + health + ", MP: " + magicPower);
				/*Combat Menu*/
				System.out.println("What action do you want to perform?");
				//Option 1
				System.out.println("	1.) Sword Attack");
				//Option 2
				System.out.println("	2.) Cast Spell");
				//Option 3
				System.out.println("	3.) Charge Mana");
				//Option 4
				System.out.println("	4.) Run Away");
		
			
				int action = input.nextInt();

				//Combat Calculation Loop Begins 
				if(action == 1){
					//Invoke Melee Method
					swordAttack();
				}

				else if (action == 2){
					//Invoke Magic Method
					magicAttack();
				}

				else if (action == 3){
					//Invoke Charge Method
					chargeMana();
				}

				else if (action == 4){
					//Flee Option
					gameStatus = false;
					System.out.println("You run away!");
				}

				else if (action < 1 || action > 4){
					//Invalid options 
					System.out.println("This is not a valid option");
				}

				if (health <= 0){
					gameStatus = false;
					System.out.println("You have been defeated by " + monsterName + "!");
				}

				if (monsterHealth <= 0){
					gameStatus = false;
					System.out.println("You have defeated the " + monsterName);
				}
		}// Combat Loop ends 

	}// end combat manager

	//Start Sword Attack Method
	public static void swordAttack (){
		//Calculate Random Damage for Hero
		attackPower = 1 + randomNumbers.nextInt(attackPower);
		//Monster health after attack
		monsterHealth = monsterHealth - attackPower;
		System.out.println("You have attacked the Monster with " + attackPower + " damage.");

		//if monster is alive attack player
		if (monsterHealth > 0)
			monsterAttack = 1 + randomNumbers.nextInt(monsterAttack);
			health = health - monsterAttack;
			System.out.println("You have been attacked for " + monsterAttack + " damage.");


	}// End Sword Attack Method 

	//Start Magic Method 
	public static void magicAttack (){
		//Create a situation for the player to use magic power 
		if (magicPower >= 3){
			//monster health is split in half when struck by magic
			monsterHealth = monsterHealth/2;
			System.out.println("You cast a spell on the monster.");
			magicPower = magicPower - 3;
			
			if (monsterHealth > 0){
				monsterAttack = 1 + randomNumbers.nextInt(monsterAttack);
				health = health - monsterAttack;
				System.out.println("You have been attacked for " + monsterAttack + " damage.");
			}
			
		}
		
		else if (magicPower < 3){
			//Repot not enough mana
			System.out.println("You do not have enough mana.");

		}

	}// End Magic Method

	// Start Charge option 
	public static void chargeMana(){
		//Increment Magic Power
		magicPower = 1 + magicPower;
		System.out.println("You focus and charge your mana.");

		//Monster Attack
		if (monsterHealth > 0){
				monsterAttack = 1 + randomNumbers.nextInt(monsterAttack);
				health = health - monsterAttack;
				System.out.println("You have been attacked for " + monsterAttack + " damage.");
			}

	}//End Charge Option

} //end class

