import java.util.Scanner;
/**
*
* author: Silvino Pina
*
* version: 10/31/17
*
*/

public class Runner{
	//main method 
	 public static void main(String[] args){
	 	//create dungeon for game 
	 	Dungeon map = new Dungeon();
		//setting current room
		Rooms currentroom = map.getRoom0();
  		//string for error
        String error = ("No Doorway there.");
        //Setting up user input for movement
        Scanner input = new Scanner(System.in);
        
        
        boolean Game = true;
        while (Game){

            //Print out current info and ask for input
            System.out.println(currentroom);
            System.out.println("Which direction? ");
            char response = input.nextLine().charAt(0);
        
        if(response == 'n'|| response == 'N'){
            if(currentroom.getNorth() != null){
                currentroom = currentroom.getNorth();
            }
            else{
                System.out.println(error);
            }

        }//end loop
        
         if(response == 'e'|| response == 'E'){
            if(currentroom.getEast() != null){
                currentroom = currentroom.getEast();
            }
            else{
                System.out.println(error);
            }


        }//end loop

        if(response == 's'|| response == 'S'){
            if(currentroom.getSouth() != null){
                currentroom = currentroom.getSouth();
            }
            else{
                System.out.println(error);
            }


        }//end loop

        if(response == 'w'|| response == 'W'){
            if(currentroom.getWest() != null){
                currentroom = currentroom.getWest();
            }
            else{
                System.out.println(error);
            }

        }//end loop


        else if(response == 'q' || response =='Q'){
            Game = false;
            System.out.println("You have decided to leave.");
        }
        }// End game loop

 	}//end main method
}//end class