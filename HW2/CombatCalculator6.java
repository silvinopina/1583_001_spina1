import java.util.Scanner;
/**	
 * This a Simple Combat Calculation Game 
 *
 * @author Silvino Pina	
 * @version 9/8/17
 */


public class CombatCalculator6{
	public static void main(String[] args){ 
		Scanner input = new Scanner(System.in);
		/*Monster data variable*/
		//Monster Name
		String name = "goblin";
		//Monster Health
		int MonsterHealth = 100;
		//Moster Attack Power
		int MonsterAttack = 15;

		/*Hero Data Variabeles*/
		//Hero Health
		int HeroHealth=100;
		//Hero Attack Power
		int HeroAttack=12;
		//Hero Magic Power
		int HeroMagic=0;
		/*Loop Control*/
		//Declare Loop 
		while (MonsterHealth > 0 && HeroHealth > 0){
		
			/*Report Combat Stats*/
			//Print Monster Name
			System.out.println("You are fighting a " + name + "!");
			//Print Monster Health
			System.out.println("The Monster HP: "+ MonsterHealth);
			//Print Player Health
			System.out.println("Your HP: " + HeroHealth);
			//Print Player Magic Power
			System.out.println("Your MP: " + HeroMagic);

			/*Combat Menu*/
			System.out.println("Combat Options:");
			//Option 1
			String option1 = "	1.) Sword Attack";
			System.out.println(option1);
			//Option 2
			String option2 = "	2.) Cast Spell";
			System.out.println(option2);
			//Option 3
			String option3 = "	3.) Charge Mana";
			System.out.println(option3);
			//Option 4
			String option4 = "	4.) Run Away";
			System.out.println(option4);
			System.out.print("What action do you want to perform? ");
			int action = input.nextInt();


			if(action == 1){
				//Calculate damage and monster health
				MonsterHealth = MonsterHealth - HeroAttack;
				//print attack text:
				System.out.println("You strike the " + name + " with your sword for " + HeroAttack + " damage");
			}

			else if(action == 2){
				//Calculate damege and update monster health
				MonsterHealth = MonsterHealth/2;
				//print spell message:
				System.out.println("You cast the weaken spell on the monster.");
			}

			else if (action == 3){
				//Increment magic power
				HeroMagic = HeroMagic+1
				//print charging message:
				System.out.println("You focus and charge your magic power.");
			}

			else if (action == 4){
				//Stop Combat
				HeroHealth = 0;
				//print retreat message:
				System.out.println("You run away!");
			}

			else {
				//print error message:
				System.out.println("I don't understand that command.");
			}

			if (MonsterHealth <= 0){
				MonsterHealth = 0;
				System.out.println("You defeated the " + name + "!");
			}
		
		}
	}
}