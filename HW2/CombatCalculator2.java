/**	
 * This a Simple Combat Calculation Game 
 *
 * @author Silvino Pina	
 * @version 9/8/17
 */


public class CombatCalculator2{
	public static void main(String[] args){ 
		/*Monster data variable*/
		//Monster Name
		String name = "goblin";
		//Monster Health
		int MonsterHealth = 100;
		//Moster Attack Power
		int MonsterAttack = 15;

		/*Hero Data Variabeles*/
		//Hero Health
		int HeroHealth=100;
		//Hero Attack Power
		int HeroAttack=12;
		//Hero Magic Power
		int HeroMagic=0;

		/*Report Combat Stats*/
		//Print Monster Name
		System.out.println("You are fighting a " + name + "!");
		//Print Monster Health
		System.out.println("The Monster HP: "+ MonsterHealth);
		//Print Player Health
		System.out.println("Your HP: " + HeroHealth);
		//Print Player Magic Power
		System.out.println("Your MP: " + HeroMagic);
	}
}