import java.util.Scanner;
/** 
*This is simple text based adventure game
*
*
*
* @author Silvino Pina
* @version 10/20/17
*/


public class AdventureTime4{
	//Setting number of rooms
	private int Number_of_Rooms = 7;
	
	//Giving the direction values 
	final private static int North = 0;
 	final private static int East = 1;
	final private static int West = 3;
	final private static int South = 2;
	
	//creating arrays for rooms and descrpition
	static int [][] exitsRoom = new int [7][4];
	static String [] descrRoom = new String [7];
	
	public static void main(String[] args){
		exitsRoom = fillArrayWithBuild();
		descrRoom = fillArrayWithBuild2();
		gameLoop();
	}//end main method

	//Creating Method for arrays that will hold value for room directions
	public static int[][] fillArrayWithBuild(){
		//giving the room arrays values for directions
		int[][] roomExits = {{3,1,-1,-1},
				{4,2,0,-1},
				{5,-1,1,-1},
				{-1,4-1,0},
				{6,5,3,1},
				{-1,-1,4,2},
				{-1,-1,-1,4}};
			// return for room exits
			return roomExits;
		
	}// end room array method
	
	//Creating Room description array
	public static String[] fillArrayWithBuild2(){
		String [] roomDescr ={
			"You are in the guest bedroom, exits are north and east.",
			"You are in the South Hall, exits are north, east, and west.",
			"You are in the Dining Room, exits are north and west.",
			"You are in the Master Bedroom, exits are east and south.",
			"You are in the North Hall, exits are north, east, west, and south.",
			"You are in the Kitche, exits are west and south.",
			"You are in the Balcony exits are south.",
		};
		//return room descriptions 
		return roomDescr;
	}// End Room Description Array

	//Game movenment method
	public static void gameLoop(){
		//Varialbe for current room and direction
		int currentroom = 0;
		int dir = 0;
		
		//string for error
		String error = ("No Doorway there.");
		//Setting up user input for movement
		Scanner input = new Scanner(System.in);
		
		
		boolean Game = true;
		while (Game){

			//Print out current info and ask for input
			System.out.println(descrRoom[currentroom]);
			System.out.println("Which direction? ");
			char response = input.nextLine().charAt(0);
		
		if(response == 'n'|| response == 'N'){
			if(exitsRoom[currentroom][North] != -1){
				dir = North;
				currentroom = exitsRoom[currentroom][dir];
			}
			else{
				System.out.println(error);
			}

		}//end loop
		
		if(response == 'e'|| response == 'E'){
			if(exitsRoom[currentroom][East] != -1){
				dir = East;
				currentroom = exitsRoom[currentroom][dir];
			}
			else{
				System.out.println(error);
			}

		}//end loop

		if(response == 's'|| response == 'S'){
			if(exitsRoom[currentroom][South] != -1){
				dir = South;
				currentroom = exitsRoom[currentroom][dir];
			}
			else{
				System.out.println(error);
			}

		}//end loop

		if(response == 'w'|| response == 'W'){
			if(exitsRoom[currentroom][West] != -1){
				dir = West;
				currentroom = exitsRoom[currentroom][dir];
			}
			else{
				System.out.println(error);
			}

		}//end loop


		else if(response == 'q' || response =='Q'){
			Game = false;
			System.out.println("You have decided to leave.");
		}
		}// End game loop

	}// Game method
}//end class 
