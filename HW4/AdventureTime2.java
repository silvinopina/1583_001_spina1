import java.util.Scanner;
/** 
*This is simple text based adventure game
*
*
*
* @author Silvino Pina
* @version 10/20/17
*/


public class AdventureTime2{
	
	static int currentroom = 0;
	int Number_of_Rooms = 7;
	int North = 0;
 	int East = 1;
	int West = 2;
	int Souht = 3;

	
	public static void main(String[] args){
		int [][] exitsRoom = fillArrayWithBuild();
		String [] descrRoom = fillArrayWithBuild2();
		gameLoop();
	}//end main method

	
	public static int[][] fillArrayWithBuild(){
		int[][] roomExits = new int [7][4];
		for (int i = 0; i<7; i++){
			for (int j = 0; j<4; j++){
				
				roomExits [0][0] = 3;
				roomExits [0][1] = 1;
				roomExits [0][2] =-1;
				roomExits [0][3] =-1;

				roomExits [1][0] = 4;
				roomExits [1][1] = 2;
				roomExits [1][2] = 0;
				roomExits [1][3] =-1;

				roomExits [2][0] = 5;
				roomExits [2][1] =-1;
				roomExits [2][2] = 1;
				roomExits [2][3] =-1;

				roomExits [3][0] =-1;
				roomExits [3][1] = 4;
				roomExits [3][2] =-1;
				roomExits [3][3] = 0;

				roomExits [4][0] = 6;
				roomExits [4][1] = 5;
				roomExits [4][2] = 3;
				roomExits [4][3] = 1;

				roomExits [5][0] =-1;
				roomExits [5][1] =-1;
				roomExits [5][2] = 4;
				roomExits [5][3] = 2;

				roomExits [6][0] =-1;
				roomExits [6][1] =-1;
				roomExits [6][2] =-1;
				roomExits [6][3] = 4;
			}
		}
		return roomExits;
	}// Build Room Exit Array
	
	public static String[] fillArrayWithBuild2(){
		String [] roomDescr ={
			("You are in the guest bedroom, exits are north and east."),
			("You are in the South Hall, exits are north, east, and west."),
			("You are in the Dining Room, exits are north and west."),
			("You are in the Master Bedroom, exits are east and south."),
			("You are in the North Hall, exits are north, east, west, and south."),
			("You are in the Kitche, exits are west and south."),
			("You are in the Balcony exits are south."),
		};
		return roomDescr;
	}// Build Room Description Array

	public static void gameLoop(){
		int [][] exitsRoom = fillArrayWithBuild();
		String [] descrRoom = fillArrayWithBuild2();
		Scanner input = new Scanner(System.in);
		
		boolean loop = true;
		
		while(loop){ 
			int current = exitsRoom[0][0];
			String Current = descrRoom[0];
			String dir = input.next();
			System.out.println(Current);
			System.out.println("Which direction?");
		
			if (dir == 'q'){
				loop = false;
				System.out.println("You leave the castle and go home.");
			}
			

		}//end loop

	}
}//end class 
